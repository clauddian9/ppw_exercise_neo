from django.urls import path
from lab_6.views import index, add_status, about, book_list, book_data

urlpatterns = [
    path('', index, name = 'home'),
    path('home/', index, name = 'home'),
    path('add_status/', add_status, name = 'add_status'),
    path('about/', about, name = 'about'),
    path('book_list/', book_list, name = 'book_list'),
    path('book_data/<str:url>/', book_data, name='book_data'),
]