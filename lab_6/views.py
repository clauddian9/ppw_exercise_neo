from django.shortcuts import get_object_or_404
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse 
import requests
import json

from .forms import Status_Form
from .models import Status

response = {}
def index(request):
    response['author'] = "Clouddian Fazalmuttaqin"
    status_list = Status.objects.all()
    response['status_list'] = status_list
    html = 'landing_page.html'
    response['status_form'] = Status_Form
    return render(request, html, response)

def add_status(request):
    status = Status_Form(request.POST or None)
    if request.method == 'POST' and status.is_valid():
        response['my_status'] = request.POST['my_status']
        new_status = Status(my_status = response['my_status'])
        new_status.save()
        return HttpResponseRedirect('/home/')
    else:
        return HttpResponseRedirect('/home/')

def about(request):
    return render(request, 'about.html')

def book_list(request):
    return render(request, 'book_list.html')

def book_data(request, url):
    book_object = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + url)
    
    book_json = book_object.json()
    book_data = [];
    for data in book_json['items'] :
        item = {}
        
        try:
            item['cover'] = data['volumeInfo']['imageLinks']['smallThumbnail']
        except:
            item['cover'] = "none"

        try:
            item['title'] = data['volumeInfo']['title']
        except:
            item['title'] = "none"

        try:
            item['authors'] = ", ".join(data['volumeInfo']['authors'])
        except:
            item['authors'] = "none"

        try:
            item['year_published'] = data['volumeInfo']['publishedDate'][:4]
        except:
            item['year_published'] = "none"
            
        book_data.append(item)
        
    return JsonResponse({"data" : book_data})

