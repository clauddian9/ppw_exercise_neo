from django import forms

class Status_Form(forms.Form):
    error_message = {
        'required': 'Please fill this field',
    }

    status_attrs = {
        'type': 'text',
        'class': 'form-control todo-form-textarea',
    }

    my_status = forms.CharField(
        required = True,
        widget = forms.Textarea(attrs = status_attrs),
        max_length = 300,
        label = '')