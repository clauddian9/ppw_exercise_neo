# from django.http import HttpRequest
# from django.test import TestCase
# from django.test import Client
# from django.urls import resolve
# import lab_6.views as views
# from .models import Status
# from .forms import Status_Form

# from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.chrome.options import Options


# class Lab6UnitTest(TestCase):

#     def test_lab6_url_is_exist(self):
#         response = Client().get('/home/')
#         self.assertEqual(response.status_code, 200)

#     def test_lab6_url_is_not_exist(self):
#         response = Client().get('/error/')
#         self.assertEqual(response.status_code, 404)
        
#     def test_book_list_url_is_exist(self):
#         response = Client().get('/book_list/')
#         self.assertEqual(response.status_code, 200)
        
#     def test_book_data_url_is_exist(self):
#         response = Client().get('/book_data/quilting')
#         self.assertEqual(response.status_code, 301)

#     def test_lab6_using_index_func(self):
#         found = resolve('/home/')
#         self.assertEqual(found.func, views.index)
        
#     def test_book_list_using_book_list_func(self):
#         found = resolve('/book_list/')
#         self.assertEqual(found.func, views.book_list)
    
#     def test_book_data_using_book_data_func(self):
#         found = resolve('/book_data/quilting/')
#         self.assertEqual(found.func, views.book_data)

#     def test_lab6_using_landing_page_template(self):
#         response = Client().get('/home/')
#         self.assertTemplateUsed(response, 'landing_page.html')

#     def test_lab6_landing_page_is_complete(self):
#         request = HttpRequest()
#         response = views.index(request)
#         html_response = response.content.decode('utf8')
#         self.assertIn("Hello apa kabar?", html_response)
        
#     def test_book_list_using_book_list_page_template(self):
#         response = Client().get('/book_list/')
#         self.assertTemplateUsed(response, 'book_list.html')

#     def test_model_status_can_add_new_status(self):
#         new_status = Status.objects.create(my_status = 'working on PPW')
#         counting_added_status = Status.objects.all().count()
#         self.assertEqual(counting_added_status, 1)
    
#     def test_form_status_input_has_placeholder(self):
#         form = Status_Form()
#         self.assertIn('class="form-control todo-form-textarea', form.as_p())
#         self.assertIn('id="id_my_status', form.as_p())
#         self.assertIn('type="text', form.as_p())

#     def test_form_error_message_for_blank_field(self):
#         form = Status_Form(data={'my_status': ''})
#         self.assertFalse(form.is_valid())
#         self.assertEqual(
#             form.errors['my_status'],
#             ["This field is required."]
#         )
    
#     def test_lab6_post_success_and_render_the_result(self):
#         response_post = Client().post('/add_status/', {'my_status': 'test'})
#         self.assertEqual(response_post.status_code, 302)

#         response = Client().get('/home/')
#         html_response = response.content.decode('utf8')
#         self.assertIn('test', html_response)

#     def test_lab6_post_error_and_render_the_result(self):
#         test = 'Anonymous'
#         response_post = Client().post('/add_status/', {'my_status': ''})
#         self.assertEqual(response_post.status_code, 302)

#         response = Client().get('/home/')
#         html_response = response.content.decode('utf8')
#         self.assertNotIn(test, html_response)

#     # test for the challenge
#     def test_lab6_about_url_is_exist(self):
#         response = Client().get('/about/')
#         self.assertEqual(response.status_code, 200)

#     def test_lab6_about_using_about_func(self):
#         found = resolve('/about/')
#         self.assertEqual(found.func, views.about)
    
#     def test_lab6_about_using_about_template(self):
#         response = Client().get('/about/')
#         self.assertTemplateUsed(response, 'about.html')

#     def test_lab6_about_page_have_cloud_description(self):
#         request = HttpRequest()
#         response = views.about(request)
#         html_response = response.content.decode('utf8')
#         self.assertIn('A dedicated student with proven analysis', html_response)

#     def test_lab6_about_page_have_cloud_image(self):
#         request = HttpRequest()
#         response = views.about(request)
#         html_response = response.content.decode('utf8')
#         self.assertIn('<img src="/static/images/cloud', html_response)

#     def test_lab6_about_page_have_cloud_contact(self):
#         request = HttpRequest()
#         response = views.about(request)
#         html_response = response.content.decode('utf8')
#         self.assertIn('CONTACT', html_response)

#     def test_lab6_about_page_have_cloud_education(self):
#         request = HttpRequest()
#         response = views.about(request)
#         html_response = response.content.decode('utf8')
#         self.assertIn('EDUCATION', html_response)
    
#     def test_lab6_about_page_have_cloud_skill(self):
#         request = HttpRequest()
#         response = views.about(request)
#         html_response = response.content.decode('utf8')
#         self.assertIn('SKILLS', html_response)

# # agar tidak error di gitlab
# # class Lab6FunctionalTest(TestCase):
# #     def setUp(self):
#         # gitlab
#         # chrome_options = Options()
#         # chrome_options.add_argument('--dns-prefetch-disable')
#         # chrome_options.add_argument('--no-sandbox')
#         # chrome_options.add_argument('--headless')
#         # self.selenium = webdriver.Chrome(chrome_options=chrome_options)

#         #local
#         # self.selenium  = webdriver.Chrome() 
        
#     #     super(Lab6FunctionalTest, self).setUp()

#     # def tearDown(self):
#     #     self.selenium.quit()
#     #     super(Lab6FunctionalTest, self).tearDown()

#     # def test_input_status(self):
#     #     selenium = self.selenium
#     #     # Opening the link we want to test
#     #     selenium.get('http://127.0.0.1:8000/home/')
        
#     #     # find the form element
#     #     status = selenium.find_element_by_id('id_my_status')

#     #     submit = selenium.find_element_by_id('submit')

#     #     # Fill the form with data
#     #     status.send_keys('Coba Coba')

#     #     # submitting the form
#     #     submit.send_keys(Keys.RETURN)

#     #     # check the returned result
#     #     self.assertIn('Coba Coba', selenium.page_source)

#     # def test_layout(self):
#     #     selenium = self.selenium
#     #     # Opening the link we want to test
#     #     selenium.get('http://127.0.0.1:8000/home/')

#     #     # find the header & footer element
#     #     header = selenium.find_element_by_id('header').text # 1
#     #     footer = selenium.find_element_by_id('footer').text # 2
        
#     #     # check the layout
#     #     self.assertIn('Home', header)
#     #     self.assertIn('About', header)
#     #     self.assertEqual('Believe more in millions dollar execution', footer)

#     # def test_style(self):
#     #     selenium = self.selenium
#     #     # Opening the link we want to test
#     #     selenium.get('http://127.0.0.1:8000/home/')

#     #     # find the status-list box and submit button element
#     #     submit = selenium.find_element_by_id('submit') # 1
#     #     status_list = selenium.find_element_by_id('list-status') # 2
        
#     #     # get the CSS of the element
#     #     submit_text_color = submit.value_of_css_property("color")
#     #     status_list_background = status_list.value_of_css_property("background-color")
#     #     status_list_alignment = status_list.value_of_css_property("text-align")

#     #     # check the style
#     #     self.assertEqual('rgba(33, 37, 41, 1)', submit_text_color)
#     #     self.assertEqual('rgba(255, 164, 17, 1)', status_list_background)
#     #     self.assertEqual('left', status_list_alignment)

#     # def change_background(self):
#     #     selenium = self.selenium
#     #     # Opening the link we want to test
#     #     selenium.get('http://127.0.0.1:8000/about/')

#     #     buttonChangeDark = selenium.find_element_by_id("dark-on")
#     #     my_bio = selenium.find_element_by_id('my_bio')

#     #     buttonChangeDark.click()
#     #     my_bio_color = my_bio.value_of_css_property("color")
#     #     my_bio_background = my_bio.value_of_css_property("background-color")
#     #     self.assertEqual('rgba(255, 255, 255, 1)', my_bio_color)
#     #     self.assertEqual('rgba(8, 103, 136, 1)', my_bio_background)



