from django.http import HttpRequest
from django.test import TestCase
from django.test import Client
from django.urls import resolve
import json
import lab_11.views as views

class Lab11UnitTest(TestCase):

    def test_lab11_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_lab11_url_is_not_exist(self):
        response = Client().get('/error/')
        self.assertEqual(response.status_code, 404)

    def test_lab11_book_url_is_exist(self):
        response = Client().get('/book/')
        self.assertEqual(response.status_code, 200)

    def test_lab11_filled_google_book_url_is_exist(self):
        response = Client().get('/google_book/cooking/')
        self.assertEqual(response.status_code, 200)

    def test_lab11_logout_url_is_exist(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 302)

    def test_lab11_using_index_func(self):
        found = resolve('/book/')
        self.assertEqual(found.func, views.index)
        
    def test_google_book_using_google_book_func(self):
        found = resolve('/google_book/cooking/')
        self.assertEqual(found.func, views.google_book)
    
    def test_logout_using_logout_func(self):
        found = resolve('/logout/')
        self.assertEqual(found.func, views.logout)

    def test_favorite_using_logout_func(self):
        found = resolve('/favorite/')
        self.assertEqual(found.func, views.favorite)

    def test_lab11_using_index_template(self):
        response = Client().get('/book/')
        self.assertTemplateUsed(response, 'lab_11/index.html')

    # def test_lab11_post_success(self):
    #     response = self.client.get('/google_book/')
    #     book_json = response.json()
    #     id = book_json['items'][0]['id']
    #     response = self.client.post('/favorite/', json.dumps({'id': id, 'favorited': True}), content_type='application/json')
    #     self.assertEqual(response.status_code, 200)

    def test_lab11_post_fail(self):
        response = self.client.post('/favorite/', json.dumps({'id': 'ckKQWGpJ_1kC'}), content_type='application/json')
        self.assertEqual(response.status_code, 400)
