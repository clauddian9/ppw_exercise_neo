from django.shortcuts import get_object_or_404
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse 
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import logout as logout_user
import requests
import json

response = {}
def index(request):
    return render(request, 'lab_11/index.html')

def logout(request):
    logout_user(request)
    return HttpResponseRedirect('/book/')

def google_book(request, url='quilting'):
    book_object = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + url)
    
    book_json = book_object.json()
    book_data = []
    star_count = 0
    for data in book_json['items'] :
        favorited = request.session.get(data['id'], False)
        item = {}

        item['id'] = data['id']
        if favorited:
            item['favorited'] = True
            star_count += 1
        else:
            item['favorited'] = False
        
        try: item['cover'] = data['volumeInfo']['imageLinks']['smallThumbnail']
        except: item['cover'] = "none"

        try: item['title'] = data['volumeInfo']['title']
        except: item['title'] = "none"

        try: item['authors'] = ", ".join(data['volumeInfo']['authors'])
        except: item['authors'] = "none"

        try: item['year_published'] = data['volumeInfo']['publishedDate'][:4]
        except: item['year_published'] = "none"
            
        book_data.append(item)
    book_data[1] = star_count
    return JsonResponse({"data" : book_data})

@csrf_exempt
def favorite(request):
    book_data = json.loads(request.body.decode())
    try:
        id = book_data['id']
        favorited = book_data['favorited']
        request.session[id] = favorited
        return HttpResponse(status=204)
    except KeyError:
        return HttpResponse(status=400)
