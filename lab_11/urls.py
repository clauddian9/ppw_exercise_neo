from django.urls import path
from lab_11.views import index, google_book, favorite, logout

urlpatterns = [
    path('', index, name = 'book'),
    path('book/', index, name = 'book'),
    path('google_book/<str:url>/', google_book, name='google_book'),
    path('favorite/', favorite, name='favorite'),
    path('logout/', logout, name='logout'),
]