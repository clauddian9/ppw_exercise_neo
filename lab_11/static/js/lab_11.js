var bookType = "quilting";
var count_favorite = 0;

update_favorite();
function update_favorite() {
    $('#favorite-count').text(count_favorite);
};

function resetBook() {
    $('#table-body').empty()
    count_favorite = 0;
    update_favorite();
}

function generateBook() {
    $.ajax({ 
        url : "/google_book/" + bookType, 
        dataType : "json",
        success: function (response) {
            resetBook()
            var book_data = response["data"];
            for (var i = 0; i < book_data.length; i++) {
                var bintang = "☆";
                if (book_data[i]['favorited']) {
                    bintang = "★";
                    count_favorite++;
                } 

                var table_content = "<tr>" +
                    "<td>" + '<img src="'+ book_data[i]['cover'] +'" class="img-fluid rounded mx-auto d-block">' + "</td>" +
                    "<th scope=\"row\">" + book_data[i]['title'] + "</th>" +
                    "<td>" + book_data[i]['authors'] + "</td>" +
                    "<td>" + book_data[i]['year_published'] + "</td>" +
                    "<td><p class=\"favorite star-symbol\" id='" + book_data[i]['id'] + "'>" + bintang + "</p></td>" +
                "</tr>";
                $('#table-body').append(table_content);
            };
            update_favorite();
        },
        
        error: function (error) {
            var table_content = "<tr><td colspan=\"5\" class = \"text-center\">JSON failed to loaded :(</td></tr>";
            $('#table-body').append(table_content);
        },
    });
}

function search() {
    bookType = $('#text-search').val()
    generateBook()
}


$(document).ready(function () {
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
          }
        }
        return cookieValue;
    }
      
    //coba cari tahu, mengapa kita perlu mengambil csrftoken dari cookie
    var csrftoken = getCookie('csrftoken');
    //token yang ada di variable csrftoken kalian masukan dipassing ke dalam fungsi xhr.setRequestHeader("X-CSRFToken", csrftoken);
    //coba cari tahu, kenapa kita perlu mensetup csrf token terlebih dahulu sebelum melakukan request post ke views django

    //yang disarankan dari dokumentasi django nya
    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    
    $.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
        xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
    });
    
    generateBook()

    $(document).on('click', '.favorite', function(){
        if($(this).text() == "☆"){
            count_favorite++;
            update_favorite();
            $(this).text("★");
            $.ajax({
                url: '/favorite/',
                type: 'POST',
                data: JSON.stringify({id: $(this).attr('id'), favorited: true}),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
            })        
        }
        else{
            count_favorite--;
            update_favorite();
            $(this).text("☆");
            $.ajax({
                url: '/favorite/',
                type: 'POST',
                data: JSON.stringify({id: $(this).attr('id'), favorited: false}),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
            })        
        }
    });
});