from django.shortcuts import render
from .models import Subscriber
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse

def index(request):
    return render(request, 'lab_10/registration.html')

def new(request):
    if request.method == 'POST':
        name = request.POST['name']
        email = request.POST['email']
        password = request.POST['password']

        new_subscriber = Subscriber.objects.create(
            name = name,
            email = email,
            password = password,
        )
        new_subscriber.save()

        return HttpResponse('')
    else:
        return HttpResponseRedirect('/registration/')
    
def email_checking(request):
    emailIsExist = False
    if 'email' in request.POST:
        count = Subscriber.objects.filter(email = request.POST['email']).count()
        if count > 0: emailIsExist = True
    return JsonResponse({"email_is_exist": emailIsExist}, content_type = 'application/json')

def subscriber_list(request):
    return render(request, 'lab_10/subscriber_list.html')

def subscriber_data(request):
    subscriber_object = Subscriber.objects.all().values('name', 'email')
    subscriber_list = list(subscriber_object)

    return JsonResponse(subscriber_list, safe = False)

# def password_confirmation(request):
#     def first(query):
#         try:
#             return query.all()[0]
#         except:
#             return None

#     if request.method == 'POST':
#         email = request.POST['email']
#         password = request.POST['password']
#         objects = first(Subscriber.objects.filter(email=email))
#         if password == objects.password:
#             Subscriber.objects.filter(email=email).delete()
#             return JsonResponse({"password_is_valid": True}, content_type = 'application/json')
#         else:
#             return JsonResponse({"password_is_valid": False}, content_type = 'application/json')