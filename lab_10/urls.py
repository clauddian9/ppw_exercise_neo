from django.urls import path
from lab_10.views import index, new, email_checking, subscriber_list, subscriber_data

urlpatterns = [
    path('', index, name = 'registration'),
    path('new/', new, name = 'new'),
    path('email_checking/', email_checking, name = 'email_checking'),
    path('subscriber_list/', subscriber_list, name = 'subscriber_list'),
    path('subscriber_data/', subscriber_data, name = 'subscriber_data'),
    # path('password_confirmation/', password_confirmation, name = 'password_confirmation'),
]