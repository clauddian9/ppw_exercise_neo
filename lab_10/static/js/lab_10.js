var emailIsValid = false;
var nameIsValid = false;
var passwordIsValid = false;
var wantToDelete = "";

function emailValidation() {
    var inputEmail = $('#inputEmail').val();
    var csrfmiddlewaretoken = $('[name=csrfmiddlewaretoken]').val();
    $.ajax({
        method : "POST",
        url : "/registration/email_checking/",
        data : {email:inputEmail},
        dataType : "json",
        headers: {
            "X-CSRFToken": csrfmiddlewaretoken
        },
        success : function (datajson) {
            if (datajson.email_is_exist) {
                emailIsValid = false;
                $('#invalid-email').text("Email is already exist");
                $('#invalid-email').css({"color":"red","text-align":"left"});
                $('#inputEmail').removeClass('is-valid');
                $('#inputEmail').addClass('is-invalid');

            } else if (inputEmail.length == 0) {
                emailIsValid = false;
                $('#invalid-email').text("Email is empty");
                $('#invalid-email').css({"color":"red","text-align":"left"});
                $('#inputEmail').removeClass('is-valid');
                $('#inputEmail').addClass('is-invalid');
            } else if (!isEmail(inputEmail)){
                emailIsValid = false;
                $('#invalid-email').text("Please enter a valid e-mail");
                $('#invalid-email').css({"color":"red","text-align":"left"});
                $('#inputEmail').removeClass('is-valid');
                $('#inputEmail').addClass('is-invalid');
            } else {
                emailIsValid = true;
                $('#invalid-email').text("");
                $('#inputEmail').removeClass('is-invalid');
                $('#inputEmail').addClass('is-valid');
            }
        },

        error: function (error) {
          console.log("Data JSON tidak berhasil diakses");
        },
    });
    buttonIsVisible()
}

// function deleteSubscriber(id) {
//     wantToDelete = id;
//     console.log(id);
//     $('#exampleModal').modal('show');
// }

// function confirmPassword() {
//     var confirmPassword = $('#password-confirm').val();
//     var csrfmiddlewaretoken = $('[name=csrfmiddlewaretoken]').val();
//     var idEmail = '#' + wantToDelete;
//     console.log('' + idEmail +'');
//     var confirmEmail = $('' + idEmail).val();
//     var lalal = $('#email0').val();
//     console.log(lalal);
//     console.log(confirmEmail);
//     $.ajax({
//         type:'POST',
//         url:'/registration/password_confirmation/',
//         headers: {
//             "X-CSRFToken": csrfmiddlewaretoken
//         },
//         data:{
//             email:confirmEmail,
//             password:confirmPassword,
//             csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val()
//         },
//         dataType : "json",
//         sucess:function(datajson) {
//             $('#exampleModal').modal('hide');
//             if (datajson.password_is_valid) {
//                 alert("Successfully unsubscribe!");
//             } else {
//                 alert("Your password is wrong!");
//             }
//         }
//     });
// }

function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function nameValidation() {
    var inputName = $('#inputName').val()
    if (inputName.length > 0 && inputName.length <= 40) {
        nameIsValid = true;
        $('#invalid-name').text("");
        $('#inputName').removeClass('is-invalid');
        $('#inputName').addClass('is-valid');
    } else {
        nameIsValid = false;
        $('#invalid-name').text("Name is empty or more than 40");
        $('#invalid-name').css({"color":"red","text-align":"left"});
        $('#inputName').removeClass('is-valid');
        $('#inputName').addClass('is-invalid');
    }
    buttonIsVisible()
}

function passwordValidation() {
    var inputPassword = $('#inputPassword').val()
    if (inputPassword.length > 0 && inputPassword.length <= 20) {
        passwordIsValid = true;
        $('#invalid-password').text("");
        $('#inputPassword').removeClass('is-invalid');
        $('#inputPassword').addClass('is-valid');
    } else {
        passwordIsValid = false;
        $('#invalid-password').text("Password is empty or more than 20");
        $('#invalid-password').css({"color":"red","text-align":"left"});
        $('#inputPassword').removeClass('is-valid');
        $('#inputPassword').addClass('is-invalid');
    }
    buttonIsVisible()
}

function buttonIsVisible() {
    if (emailIsValid && passwordIsValid && nameIsValid) {
        $('#submit').removeClass('disabled');
    } else if (!$('#submit').hasClass('disabled')) {
        $('#submit').addClass('disabled');
    }
}

$(document).ready(function () {
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
                }
            }
        }
        return cookieValue;
    }
    
    var csrftoken = getCookie('csrftoken');
    function csrfSafeMethod(method) {
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

    $(document).on('submit', '#form-registration',function(e) {
        if(!$('#submit').hasClass('disabled')) {
            e.preventDefault();
            $.ajax({
                type:'POST',
                url:'/registration/new/',
                data:{
                    name:$('#inputName').val(),
                    email:$('#inputEmail').val(),
                    password:$('#inputPassword').val(),
                    csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val()
                },
                sucess:function() {
                }
            });
            $('#inputName').val("")
            $('#inputEmail').val("")
            $('#inputPassword').val("")
            alert("New user has been created!");
            emailIsValid = false;
            nameIsValid = false;
            passwordIsValid = false;
        } else {
            return false;
        }
    });

    $.ajax({ 
        url : "/registration/subscriber_data/", 
        dataType : "json",
        success: function (response) { 
            var email_id = '';
            for (var i = 0; i < response.length; i++) {
                email_id = 'email' + i;
                var table_content = "<tr>" +
                    "<th scope=\"row\">" + (i+1) + "</th>" +
                    "<td>" + response[i].name + "</td>" +
                    "<td><p id = '" + email_id + "'>" + response[i].email + "</p></td>" +
                    "<td><button class='btn btn-lg btn-primary btn-block' id='unsubscribe'>Unsubscribe</button></td>" +
                "</tr>";
                $('#table-body').append(table_content);
            };
        },
        
        error: function (error) {
            var table_content = "<tr><td colspan=\"4\" class = \"text-center\">JSON failed to loaded :(</td></tr>";
            $('#table-body').append(table_content);
        },
    });
});